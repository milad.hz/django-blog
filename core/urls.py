"""core URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
	https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
	1. Add an import:  from my_app import views
	2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
	1. Add an import:  from other_app.views import Home
	2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
	1. Import the include() function: from django.urls import include, path
	2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth.views import PasswordChangeView, PasswordChangeDoneView
from django.urls import path, include, re_path

from accounts.views import Login, SignUpView, activate_account

urlpatterns = [
	path('', include('blog.urls')),
	path('', include('django.contrib.auth.urls')),
	path('login/', Login.as_view(), name='login'),
	path('signup/', SignUpView.as_view(), name='signup'),
	path('activate/<slug:uidb64>/<slug:token>/',activate_account, name='activate_account'),
	path('admin/', admin.site.urls),
	path('accounts/', include('accounts.urls')),
	path('accounts/profile/password_change/', PasswordChangeView.as_view(), name='password_change'),
	path('accounts/profile/password_change/done/', PasswordChangeDoneView.as_view(), name='password_change_done'),

	
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
