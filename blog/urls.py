from django.urls import path
from .views import ArticleListView, ArticleDetailView, CategoryListView, AuthorListView, ArticlePreviewView

app_name = 'blog'

urlpatterns = [
	path('', ArticleListView.as_view(), name='home'),
	path('page/<int:page>', ArticleListView.as_view(), name='home'),
	path('article/<slug:slug>', ArticleDetailView.as_view(), name='post_detail'),
	path('preview/<int:pk>', ArticlePreviewView.as_view(), name='post_preview'),
	path('category/<slug:slug>', CategoryListView.as_view(), name='category_posts'),
	path('category/<slug:slug>/page/<int:page>', CategoryListView.as_view(), name='category_posts'),
	path('author/<slug:username>', AuthorListView.as_view(), name='author_posts'),
	path('author/<slug:username>/page/<int:page>', AuthorListView.as_view(), name='author_posts'),
]

