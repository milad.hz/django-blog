from django.db import models
from django.urls import reverse
from django.utils.html import format_html
from django.utils import timezone

from accounts.models import User
from extensions.utils import jalali_convertor

STATUS_CHOICES = (
    ('d', 'پیش نویس'),
    ('p', 'منتشر شده'),
    ('w', 'در انتظار تایید'),
    ('b', 'برگشت داده شده'),
)

# Custom Managers
class ArticleManager(models.Manager):
    def published(self):
        return self.filter(status='p')

class CategoryManager(models.Manager):
    def active(self):
        return self.filter(status=True)

# Models
class Category(models.Model):
    title = models.CharField(max_length=200, verbose_name='عنوان دسته‌بندی')
    slug = models.SlugField(max_length=100, unique=True, verbose_name='آدرس')
    parent = models.ForeignKey('self', default=None, null=True, blank=True, on_delete=models.SET_NULL, related_name='children', verbose_name='زیردسته')
    status = models.BooleanField(default=True, verbose_name='آیا نمایش داده شود؟',)
    position = models.IntegerField(verbose_name='پوزیشن')

    class Meta:
        verbose_name = 'دسته‌بندی'
        verbose_name_plural = 'دسته‌بندی‌ها'
        ordering = ['parent__id','position']

    def __str__(self):
        return self.title

    objects = CategoryManager()

class Article(models.Model):
    author = models.ForeignKey(User, null=True, on_delete=models.SET_NULL, related_name='articles', verbose_name='نویسنده')
    title = models.CharField(max_length=200, verbose_name='عنوان')
    slug = models.SlugField(max_length=100, unique=True, verbose_name='آدرس')
    category = models.ManyToManyField(Category, verbose_name='دسته‌بندی', related_name='articles')
    description = models.TextField(verbose_name='محتوا')
    thumbnail = models.ImageField(upload_to='images', verbose_name='تصویر')
    created_on = models.DateTimeField(auto_now_add=True, verbose_name = 'ایجاد شده در')
    published_on = models.DateTimeField(default=timezone.now, verbose_name = 'منتشر شده در')
    updated_on = models.DateTimeField(auto_now=True, verbose_name = 'آپدیت شده در')
    is_special = models.BooleanField(default=False, verbose_name='مقاله ویژه',)
    status = models.CharField(max_length=1, choices=STATUS_CHOICES, verbose_name='وضعیت',)

    class Meta:
        verbose_name = 'مقاله'
        verbose_name_plural = 'مقالات'
        ordering = ['-published_on']

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('accounts:article_list')

    def jpublish(self):
        return jalali_convertor(self.published_on)
    jpublish.short_description = 'منتشر شده در'

    def jcreate(self):
        return jalali_convertor(self.created_on)
    jcreate.short_description = 'ایجاد شده در'

    def jupdate(self):
        return jalali_convertor(self.updated_on)
    jupdate.short_description = 'آپدیت شده در'

    def thumbnail_tag(self):
        return format_html(f'<img src="{self.thumbnail.url}" width=120 height=95 style="border-radius:5px;">')
    thumbnail_tag.short_description = 'تصویر'

    def cat_to_str(self):
        return '، '.join([category.title for category in self.category.active()])
    cat_to_str.short_description = 'دسته‌بندی(ها)'

    objects = ArticleManager()
    
    
