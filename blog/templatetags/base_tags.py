from django import template
from ..models import Category

register = template.Library()

@register.simple_tag
def title():
    return 'وبلاگ جنگویی'

# @register.simple_tag
# def title(data='وبلاگ جنگویی'):
#     return data

@register.inclusion_tag('blog/partials/category_navbar.html')
def category_navbar():
    return {
        'categories': Category.objects.filter(status=True),
    }

@register.inclusion_tag('registration/partials/active_link.html')
def active_link(request, link_name, icon, content):
    return {
        'request': request,
        'link_name': link_name,
        'link': f'accounts:{link_name}',
        'icon': icon,
        'content': content,
    }