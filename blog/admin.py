from django.contrib import admin, messages
from django.utils.translation import ngettext

from .models import Article, Category

### Admin Panel Changes ###
admin.site.site_header = 'بخش مدیریت سایت'


### ModelAdmins ###
@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ['position','title','slug','parent','status']
    list_filter = ['status']
    search_fields = ['title', 'slug']
    prepopulated_fields = {'slug': ('title',)}
    actions = ['make_active', 'make_inactive']
    ordering = ['-status']

    def make_active(self, request, queryset):
        updated = queryset.update(status=True)
        self.message_user(request, ngettext(
            'دسته‌بندی با موفقیت فعال شد.',
            f'{updated} دسته‌بندی با موفقیت فعال شدند.',
            updated,
        ), messages.SUCCESS)
    make_active.short_description = 'فعال کردن دسته‌بندی ‌ها انتخاب شده'

    def make_inactive(self, request, queryset):
        updated = queryset.update(status=False)
        self.message_user(request, ngettext(
            'دسته‌بندی با موفقیت غیرفعال شد.',
            f'{updated} دسته‌بندی با موفقیت غیرفعال شدند.',
            updated,
        ), messages.SUCCESS)
    make_inactive.short_description = 'غیرفعال کردن دسته‌بندی ‌ها انتخاب شده'

@admin.register(Article)
class ArticleAdmin(admin.ModelAdmin):
    list_display = ['title', 'thumbnail_tag', 'slug', 'author', 'jcreate','jpublish','jupdate', 'is_special', 'status', 'cat_to_str']
    list_filter = ['created_on','published_on','status', 'author']
    search_fields = ['title', 'description']
    prepopulated_fields = {'slug': ('title',)}
    ordering = ['-status', '-published_on']
    actions = ['make_published', 'make_draft']

    def make_published(self, request, queryset):
        updated = queryset.update(status='p')
        self.message_user(request, ngettext(
            'مقاله با موفقیت منتشر شد.',
            f'{updated} مقاله با موفقیت منتشر شدند.',
            updated,
        ), messages.SUCCESS)
    make_published.short_description = 'انتشار مقالات انتخاب شده'

    def make_draft(self, request, queryset):
        updated = queryset.update(status='d')
        self.message_user(request, ngettext(
            'مقاله با موفقیت پیش‌نویس شد.',
            f'{updated} مقاله با موفقیت پیش‌نویس شدند.',
            updated,
        ), messages.SUCCESS)
    make_draft.short_description = 'پیش‌نویس کردن مقالات انتخاب شده'
