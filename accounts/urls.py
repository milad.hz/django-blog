from django.contrib.auth import views
from django.urls import path

from .views import (
    ArticleListView, 
    ArticleCreateView, 
    ArticleUpdateView, 
    ArticleDeleteView, 
    UserProfileEditView,
)

app_name = 'accounts'

urlpatterns = [
    path('', ArticleListView.as_view(), name='article_list'),
    path('articles/create/', ArticleCreateView.as_view(), name='article_create'),
    path('articles/update/<int:pk>/', ArticleUpdateView.as_view(), name='article_update'),
    path('articles/delete/<int:pk>/', ArticleDeleteView.as_view(), name='article_delete'),
    path('profile/edit/', UserProfileEditView.as_view(), name='user_profile_edit'),
    
]
