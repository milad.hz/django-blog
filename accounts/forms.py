from django import forms
from django.contrib.auth.forms import UserCreationForm

from .models import User

class UserProfileEditForm(forms.ModelForm):
	def __init__(self, *args, **kwargs):
		user = kwargs.pop('user')
		super(UserProfileEditForm, self).__init__(*args, **kwargs)

		self.fields['username'].help_text = None
		self.fields['first_name'].help_text = 'لطفا نام خود را به زبان فارسی وارد کنید.'
		self.fields['last_name'].help_text = 'لطفا نام خانوادگی خود را به زبان فارسی وارد کنید.'

		if not user.is_superuser:
			self.fields['username'].disabled = True
			self.fields['email'].disabled = True
			self.fields['vip_user'].disabled = True
			self.fields['is_author'].disabled = True

	class Meta:
		model = User
		fields = ['username', 'email', 'first_name', 'last_name', 'vip_user', 'is_author']

class SignupForm(UserCreationForm):
	email = forms.EmailField(max_length=200)
		
	class Meta:
		model = User
		fields = ('username', 'email', 'password1', 'password2')

