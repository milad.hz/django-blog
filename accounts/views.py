from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import LoginView, PasswordChangeView
from django.contrib.sites.shortcuts import get_current_site
from django.core.mail import EmailMessage
from django.http import HttpResponse
from django.shortcuts import render
from django.template.loader import render_to_string
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode

from .forms import UserProfileEditForm, SignupForm
from .models import User
from .mixins import (
	FieldsMixin,
	FormValidMixin,
	AuthorAccessMixin,
	AuthorsAccessMixin,
	SuperUserAccessMixin,
)
from .tokens import account_activation_token
from blog.models import Article

# Create your views here.
class ArticleListView(AuthorsAccessMixin, ListView):
	template_name = "registration/article_list.html"

	def get_queryset(self):
		if self.request.user.is_superuser:
			return Article.objects.all()
		else:
			return Article.objects.filter(author=self.request.user)


class ArticleCreateView(AuthorsAccessMixin, FormValidMixin, FieldsMixin, CreateView):
	model = Article
	template_name = "registration/article_create_update.html"


class ArticleUpdateView(AuthorAccessMixin, FormValidMixin, FieldsMixin, UpdateView):
	model = Article
	template_name = "registration/article_create_update.html"


class ArticleDeleteView(SuperUserAccessMixin, DeleteView):
	model = Article
	success_url = reverse_lazy('accounts:article_list')
	template_name = "registration/article_confirm_delete.html"


class UserProfileEditView(LoginRequiredMixin, UpdateView):
	model = User
	template_name = "registration/user_profile_edit.html"
	form_class = UserProfileEditForm
	success_url = reverse_lazy('accounts:user_profile_edit')

	def get_object(self):
		return User.objects.get(pk=self.request.user.pk)

	def get_form_kwargs(self):
		kwargs = super(UserProfileEditView, self).get_form_kwargs()
		kwargs.update({
			'user':self.request.user
		})
		return kwargs


class Login(LoginView):
	def get_success_url(self):
		user = self.request.user
		if user.is_superuser or user.is_author:
			return reverse_lazy('accounts:article_list')
		else:
			return reverse_lazy('accounts:user_profile_edit')


class SignUpView(CreateView):
	template_name = "registration/signup.html"
	form_class = SignupForm

	def form_valid(self, form):
		user = form.save(commit=False)
		user.is_active = False
		user.save()
		current_site = get_current_site(self.request)
		mail_subject = 'فعالسازی اکانت در سایت وبلاگ جنگویی'
		message = render_to_string('registration/activate_account_email.html', {
			'user': user,
			'domain': current_site.domain,
			'uid':urlsafe_base64_encode(force_bytes(user.pk)),
			'token':account_activation_token.make_token(user),
		})
		to_email = form.cleaned_data.get('email')
		email = EmailMessage(
			mail_subject, message, to=[to_email]
		)
		email.send()
		return HttpResponse('لینک فعالسازی به ایمیل شما ارسال شد. لطفا برای استفاده از امکانات سایت ایمیل خود را تایید کنید. <a href="/login">ورود به سایت</a>')


def activate_account(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)

    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None

    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        return HttpResponse('اکانت شما با موفقیت تایید شد. حالا می‌توانید با نام کاربری و گذرواژه خود وارد سایت شوید. <a href="/login">ورود به سایت</a>')

    else:
        return HttpResponse('لینک فعالسازی نامعتبر است.')
