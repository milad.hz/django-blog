from django.db import models
from django.utils import timezone
from django.contrib.auth.models import AbstractUser

# Create your models here.
class User(AbstractUser):
	email = models.EmailField(unique=True, verbose_name='ایمیل')
	is_author = models.BooleanField(default=False, verbose_name='وضعیت نویسندگی')
	vip_user = models.DateTimeField(default=timezone.now, verbose_name = 'کاربر ویژه تا')

	def is_vip_user(self):
		if self.vip_user > timezone.now():
			return True
		else:
			return False
	is_vip_user.short_description = 'وضعیت کاربر ویژه'
	is_vip_user.boolean = True